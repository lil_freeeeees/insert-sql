--Adding fav film to the table and giving it main info
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, rating, special_features)
VALUES ('Interstellar', 'Fantasy film about space adventures', 2014, 1, 14, 4.99, 120, 'PG-13', '{Trailers}');


--Adding 3 actors to the table
INSERT INTO actor (first_name, last_name)
VALUES
('Matthew', 'McConaughey'),
('Anne', 'Hathaway'),
('Jessica', 'Chastain');


--Adding actor and film id
INSERT INTO film_actor (actor_id, film_id)
VALUES
(233, 1010),
(234, 1010),
(235, 1010);


--Adding film to the inventory
INSERT INTO inventory (film_id, store_id)
VALUES (1010, 2);
